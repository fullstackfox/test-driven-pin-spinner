<?php

namespace Tests\Unit;

use App\Pin;
use App\Record;
use App\Board;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RecordTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function can_add_record()
    {
    	$pin = factory(Pin::class)->create();
    	$board = factory(Board::class)->create();

    	Record::addRecord($board->id, $pin->id);

    	$this->assertDatabaseHas('records', [
    		'board_id' => $board->id,
    		'pin_id' => $pin->id,
    		'pinned_at' => Carbon::now()
    	]);
    }
}
