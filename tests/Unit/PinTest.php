<?php

namespace Tests\Unit;

use App\Pin;
use App\Tack;
use App\Board;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PinTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    function it_belongs_to_many_boards()
    {
    	$pin = factory(Pin::class)->create();

        $boards = factory(Board::class, 2)->create();

        Tack::addTack($boards[0]->id, $pin->id);
        Tack::addTack($boards[1]->id, $pin->id);

        $boards->each(function ($board) use ($pin){
            $this->assertTrue($pin->boards->contains('id', $board->id));
        });
    }

    // /** @test */
    // function it_requires_a_title()
    // {
    // 	$attributes = factory(Pin::class)->raw(['title' => '']);

    //     $this->post('/pins', $attributes)->assertSessionHasErrors('title');
    // }
}
