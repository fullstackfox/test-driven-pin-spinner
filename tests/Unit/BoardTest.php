<?php

namespace Tests\Unit;

use App\Pin;
use App\Tack;
use App\Board;
use App\Record;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BoardTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    function it_has_many_pins()
    {
    	$board = factory(Board::class)->create();

        $pins = factory(Pin::class, 2)->create();

        Tack::addTack($board->id, $pins[0]->id);
        Tack::addTack($board->id, $pins[1]->id);

        $pins->each(function ($pin) use ($board){
            $this->assertTrue($board->pins->contains('id', $pin->id));
        });
    }

    /** @test */
    function get_all_available_pins()
    {
        $board = factory(Board::class)->create();
        $pins = factory(Pin::class, 3)->create();

        Tack::addTack($board->id, $pins[0]->id);
        Tack::addTack($board->id, $pins[1]->id);
        Tack::addTack($board->id, $pins[2]->id);

        Record::addRecord($board->id, $pins[2]->id);

        $this->assertTrue($board->availablePins()->contains('id', $pins[0]->id));
        $this->assertTrue($board->availablePins()->contains('id', $pins[1]->id));
        $this->assertFalse($board->availablePins()->contains('id', $pins[2]->id));
    }
}
