<?php

namespace Tests\Unit;

use App\Pin;
use App\Tack;
use App\Board;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TackTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    function can_add_tack()
    {
    	$pin = factory(Pin::class)->create();
    	$board = factory(Board::class)->create();

    	Tack::addTack($board->id, $pin->id);

    	$this->assertDatabaseHas('tacks', [
    		'board_id' => $board->id,
    		'pin_id' => $pin->id
    	]);
    }
}
