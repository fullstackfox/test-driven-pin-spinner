<?php

namespace Tests\Feature;

use App\Pin;
use App\Tack;
use App\Board;
use App\Record;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewBoardTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function user_can_view_a_board()
    {
        $board = Board::create([
            'title' => 'Code Board',
            'description' => 'A board about code',
            'link' => 'codeboard.com'
        ]);
        $pin = factory(Pin::class)->create();
        Tack::addTack($board->id, $pin->id);

        $response = $this->get('/boards/'.$board->id);

        $response->assertSee('Code Board')
                 ->assertSee('A board about code')
                 ->assertSee('codeboard.com')
                 ->assertSee($pin->id);
    }

    /** @test */
    function cannot_see_pins_that_have_been_pinned()
    {
        $board = factory(Board::class)->create();
        $pin = factory(Pin::class)->create();
        Tack::addTack($board->id, $pin->id);

        $this->assertDatabaseHas('tacks', [
            'board_id' => $board->id,
            'pin_id' => $pin->id,
        ]);

        Record::addRecord($board->id, $pin->id);

        $this->assertFalse($board->availablePins()->contains('id', $pin->id));
    }
}
