<?php

namespace Tests\Feature;

use App\Pin;
use App\Tack;
use App\Board;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MarkAsPinnedTest extends TestCase
{
	use RefreshDatabase;

    private $pin;
    private $board;
    private $tack;

    protected function setUp()
    {
        parent::setUp();
        $this->board = factory(Board::class)->create();
        $this->pin = factory(Pin::class)->create();
        $this->tack = Tack::addTack($this->board->id, $this->pin->id);
    }

    /** @test */
    function user_can_mark_pin_as_pinned()
    {
        $record = [
            'board_id' => $this->board->id,
            'pin_id' => $this->pin->id,
            'pinned_at' => Carbon::now(),
        ];

    	$this->json('POST', "/pins/records", $record);

    	$this->assertDatabaseHas('records', $record);
    }

    /** @test */
    function a_record_is_not_created_if_the_request_fails()
    {
        $record = [
            'board_id' => null,
            'pin_id' => $this->pin->id,
            'pinned_at' => Carbon::now(),
        ];

        $response = $this->json('POST', "/pins/records", $record);

        $response->assertStatus(422);
        $this->assertDatabaseMissing('records', $record);
    }
}
