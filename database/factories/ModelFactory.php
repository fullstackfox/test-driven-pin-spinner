<?php

use App\Pin;
use App\User;
use App\Tack;
use App\Board;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(Board::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'description' => $faker->paragraph,
        'link' => $faker->url,
    ];
});

$factory->define(Pin::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'description' => $faker->paragraph,
        'link' => $faker->url,
    ];
});

$factory->define(Tack::class, function (Faker $faker) {
    return [
        'board_id' => function () {
            return Board::create()->id;
        },
        'pin_id' => function () {
            return Pin::create()->id;
        },
    ];
});
