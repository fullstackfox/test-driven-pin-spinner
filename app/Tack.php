<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tack extends Model
{
    protected $guarded = [];

    public static function addTack($boardId, $pinId)
    {
    	self::create([
    		'board_id' => $boardId,
    		'pin_id' => $pinId
    	]);
    }
}
