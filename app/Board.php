<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $guarded = [];

    public function pins()
    {
        return $this->belongsToMany(Pin::class, 'tacks');
    }

    public function availablePins()
    {
    	$pins = $this->pins()
                    ->whereNotIn('pins.id', Record::pluck('pin_id')
                    ->all())->get();

        return $pins;
    }
}
