<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $guarded = [];

    public static function addRecord($boardId, $pinId)
    {
    	self::create([
    		'board_id' => $boardId,
    		'pin_id' => $pinId,
    		'pinned_at' => Carbon::now()
    	]);
    }
}
