<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pin extends Model
{
    protected $guarded = [];

    public function boards()
    {
        return $this->belongsToMany(Board::class, 'tacks');
    }
}
