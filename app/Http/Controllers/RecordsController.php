<?php

namespace App\Http\Controllers;

use App\Record;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RecordsController extends Controller
{
    public function store(Request $request)
    {
    	$request->validate([
    		'board_id' => 'required',
    		'pin_id' => 'required',
    		'pinned_at' => 'required',
    	]);

    	$record = Record::create([
    		'board_id' => request()->board_id,
    		'pin_id' => request()->pin_id,
    		'pinned_at' => Carbon::now()
    	]);
    }
}
