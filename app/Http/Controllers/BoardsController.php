<?php

namespace App\Http\Controllers;

use App\Board;
use Illuminate\Http\Request;

class BoardsController extends Controller
{
    public function show($id)
    {
        $board = Board::find($id);

        return view('boards.show', ['board' => $board]);
    }
}
