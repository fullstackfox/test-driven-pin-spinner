<?php

Route::get('/boards/{id}', 'BoardsController@show');

Route::post('/pins/records', 'RecordsController@store');
