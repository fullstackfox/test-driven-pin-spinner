<h1>{{ $board->title }}</h1>
<a href="#">{{ $board->link }}</a>
<p>{{ $board->description }}</p>

@foreach($board->availablePins()) as $pin)
    <h3>{{ $pin->title }}</h3>
@endforeach